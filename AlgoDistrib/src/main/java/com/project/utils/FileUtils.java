package com.project.utils;

import java.nio.file.FileSystems;

public class FileUtils {
    public static String getCurrentPath() {
        return FileSystems.getDefault().getPath("").toAbsolutePath().toString();
    }
}