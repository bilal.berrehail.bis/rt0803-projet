package com.project.utils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.project.model.Sommet;
import com.project.model.Branch;
import com.project.model.Graph;

public class GraphUtils {
    public static Graph generateGraphFromFile(String fileConfiguration) throws IOException {
        String contents = new String(Files.readAllBytes(Paths.get(fileConfiguration)), StandardCharsets.UTF_8);

        Graph graphFetched = new Graph();

        String valueSommets = getValueFromKey("SOMMETS", contents);
        String valueBranchs = getValueFromKey("BRANCHS", contents);

        fetchSommetsFromValue(graphFetched, valueSommets);

        fetchBranchsFromValue(graphFetched, valueBranchs);

        return graphFetched;
    }

    public static String getValueFromKey(String key, String contents) {
        String[] contentsSplitted = contents.split("[=;]");

        for (int i = 0; i < contentsSplitted.length; i++) {
            String possibleKey = contentsSplitted[i].replaceAll("[^a-zA-Z ,{}-]", "");
            if (possibleKey.equals(key)) {
                if (i + 1 < contentsSplitted.length) {
                    return contentsSplitted[i + 1];
                }
            }
        }

        return null;
    }

    public static void fetchSommetsFromValue(Graph graph, String value) {
        ArrayList<Sommet> sommetsFetched = new ArrayList<>();
        String[] valueSplitted = value.split("\\s");

        for (String valueSommet : valueSplitted) {
            if (Pattern.matches(String.format("^%s$", Sommet.patternLabel), valueSommet)) {
                Sommet sommetToCommit = new Sommet();
                sommetToCommit.setLabel(valueSommet);
                sommetsFetched.add(sommetToCommit);
            }
        }

        graph.setSommets(sommetsFetched);
    }

    public static void fetchBranchsFromValue(Graph graph, String value) {
        value = value.substring(value.indexOf("{") + 1);
        value = value.substring(0, value.indexOf("}"));

        ArrayList<Branch> branchsFetched = new ArrayList<>();
        String[] valueSplitted = value.split(",");

        for (String valueBranch : valueSplitted) {

            Matcher matcher = Pattern.compile(String.format("(%s)[\\s]*%s[\\s]*(%s)", Sommet.patternLabel,
                    Branch.patternBranch, Sommet.patternLabel)).matcher(valueBranch);

            if (matcher.find()) {
                String sommetLabelA = matcher.group(1);
                String sommetLabelB = matcher.group(2);

                Branch branchToCommit = new Branch();

                int countSommetHandled = 0;

                for (Sommet sommet : graph.getSommets()) {
                    if (sommet.getLabel().equals(sommetLabelA)) {
                        branchToCommit.setSommetA(sommet);
                        countSommetHandled++;
                    } else if (sommet.getLabel().equals(sommetLabelB)) {
                        branchToCommit.setSommetB(sommet);
                        countSommetHandled++;
                    }
                }

                if (countSommetHandled == 2) {
                    branchsFetched.add(branchToCommit);
                }
            }
        }

        graph.setBranchs(branchsFetched);
    }

    public static void setNeighborsForSommet(Graph graph, Sommet sommet) {
        ArrayList<Sommet> neighbors = new ArrayList<>();

        for (Branch branch : graph.getBranchs()) {
            if (branch.isBroken()) {
                continue;
            }

            if (branch.getSommetA().equals(sommet)) {
                neighbors.add(branch.getSommetB());
            } else if (branch.getSommetB().equals(sommet)) {
                neighbors.add(branch.getSommetA());
            }
        }

        sommet.setNeighbors(neighbors);
    }
}