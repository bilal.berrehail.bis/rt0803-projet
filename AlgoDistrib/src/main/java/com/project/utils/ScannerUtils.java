package com.project.utils;

import java.util.Scanner;

public class ScannerUtils {
    public static void pressEnter() {
        Scanner scanner = new Scanner(System.in);
        String readString = scanner.nextLine();
        while (readString != null) {
            System.out.println(readString);

            if (readString.isEmpty()) {
                break;
            }

            if (scanner.hasNextLine()) {
                readString = scanner.nextLine();
            } else {
                readString = null;
            }
        }
    }
}