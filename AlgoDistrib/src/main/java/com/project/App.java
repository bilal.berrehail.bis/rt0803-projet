package com.project;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import com.project.model.Graph;
import com.project.model.Sommet;
import com.project.utils.FileUtils;
import com.project.utils.GraphUtils;
import com.project.utils.PrettyPrinter;

public class App {
    private static Graph graph;

    public static void main(String[] args) throws IOException {
        graph = GraphUtils.generateGraphFromFile(FileUtils.getCurrentPath() + "/graph.ini");

        try {
            executeTajibnapis(graph);
        } catch (NullPointerException ex) {
            System.out.println(
                    "Un sommet n'a pas de lien, exception levé car algorithme de Tajibnapis ne peut plus s'exécuter.");
        }
    }

    private static void executeTajibnapis(Graph graph) {
        graph.runTajibnapis();
        System.out.println("Exécution finie");

        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("Commandes : repair | fail | display | end");
            System.out.print("Entrée : ");
            String input = scanner.nextLine();

            if (input.equals("repair")) {
                boolean canRepair = false;
                for (int i = 0; i < graph.getBranchs().size(); i++) {
                    if (graph.getBranchs().get(i).isBroken()) {
                        canRepair = true;
                        System.out.println(
                                String.format("[%d] %s -- %s", i, graph.getBranchs().get(i).getSommetA().getLabel(),
                                        graph.getBranchs().get(i).getSommetB().getLabel()));
                    }
                }

                if (!canRepair) {
                    System.out.println("Pas d'arête à réparer.");
                    continue;
                }

                System.out.println("Quel est l'arête que vous souhaitez réparer ?");

                int indexBranch = -1;

                try {
                    indexBranch = scanner.nextInt();
                } catch (Exception ex) {
                    System.out.println("Accepte seulement les entiers.");
                    continue;
                }

                if (indexBranch < 0 || indexBranch >= graph.getBranchs().size()) {
                    System.out.println("L'arête n'existe pas.");
                    continue;
                }
                if (graph.getBranchs().get(indexBranch).isBroken()) {
                    graph.getBranchs().get(indexBranch).setBroken(false);

                    GraphUtils.setNeighborsForSommet(graph, graph.getBranchs().get(indexBranch).getSommetA());
                    GraphUtils.setNeighborsForSommet(graph, graph.getBranchs().get(indexBranch).getSommetB());

                    graph.getBranchs().get(indexBranch).getSommetA()
                            .repairCanal(graph.getBranchs().get(indexBranch).getSommetB());
                    graph.getBranchs().get(indexBranch).getSommetB()
                            .repairCanal(graph.getBranchs().get(indexBranch).getSommetA());

                    System.out.println("L'arête a été réparé.");
                } else {
                    System.out.println("L'arête n'est pas cassée.");
                    break;
                }
            } else if (input.equals("fail")) {
                boolean canBreak = false;
                for (int i = 0; i < graph.getBranchs().size(); i++) {
                    if (!graph.getBranchs().get(i).isBroken()) {
                        canBreak = true;
                        System.out.println(
                                String.format("[%d] %s -- %s", i, graph.getBranchs().get(i).getSommetA().getLabel(),
                                        graph.getBranchs().get(i).getSommetB().getLabel()));
                    }
                }

                if (!canBreak) {
                    System.out.println("Pas d'arête à casser.");
                    continue;
                }

                System.out.println("Quel est l'arête que vous souhaitez casser ?");

                int indexBranch = -1;

                try {
                    indexBranch = scanner.nextInt();
                } catch (Exception ex) {
                    System.out.println("Accepte seulement les entiers.");
                    continue;
                }

                if (indexBranch < 0 || indexBranch >= graph.getBranchs().size()) {
                    System.out.println("L'arête n'existe pas.");
                    continue;
                }
                if (!graph.getBranchs().get(indexBranch).isBroken()) {
                    graph.getBranchs().get(indexBranch).setBroken(true);

                    GraphUtils.setNeighborsForSommet(graph, graph.getBranchs().get(indexBranch).getSommetA());
                    GraphUtils.setNeighborsForSommet(graph, graph.getBranchs().get(indexBranch).getSommetB());

                    graph.getBranchs().get(indexBranch).getSommetA()
                            .failCanal(graph.getBranchs().get(indexBranch).getSommetB());
                    graph.getBranchs().get(indexBranch).getSommetB()
                            .failCanal(graph.getBranchs().get(indexBranch).getSommetA());

                    System.out.println("L'arête a été cassé.");
                } else {
                    System.out.println("L'arête est déjà cassé.");
                }
            } else if (input.equals("display")) {
                displayVariables();
            } else if (input.equals("end")) {
                scanner.close();
                break;
            }
        }
    }

    public static void displayVariables() {
        ArrayList<String> labelsSommetArrayList = new ArrayList<>();
        ArrayList<String> DSommetArrayList = new ArrayList<>();
        ArrayList<String> NbSommetArrayList = new ArrayList<>();
        ArrayList<ArrayList<String>> ndisSommetArrayList = new ArrayList<>();

        labelsSommetArrayList.add("  Label Sommet   ");
        DSommetArrayList.add("  D");
        NbSommetArrayList.add("  Nb");

        for (Sommet sommet : graph.getSommets()) {
            labelsSommetArrayList.add(" " + sommet.getLabel() + " ");
            DSommetArrayList.add(" " + Arrays.toString(sommet.getD()) + " ");
            NbSommetArrayList.add(" " + Arrays.toString(sommet.getNb()) + " ");

            for (int i = 0; i < sommet.getNdis().length; i++) {
                if (ndisSommetArrayList.size() >= (i + 1)) {
                    ndisSommetArrayList.get(i).add(" " + Arrays.toString(sommet.getNdis()[i]) + " ");
                } else {
                    ArrayList<String> ndisElementArrayList = new ArrayList<>();
                    ndisElementArrayList.add(String.format("  ndis[%d]", i));
                    ndisElementArrayList.add(" " + Arrays.toString(sommet.getNdis()[i]) + " ");
                    ndisSommetArrayList.add(ndisElementArrayList);
                }
            }
        }

        ArrayList<ArrayList<String>> printable = new ArrayList<>();

        printable.add(labelsSommetArrayList);
        printable.add(DSommetArrayList);
        printable.add(NbSommetArrayList);

        for (ArrayList<String> ndisElementArrayList : ndisSommetArrayList) {
            printable.add(ndisElementArrayList);
        }

        String[][] printable2dArray = new String[printable.size()][];
        for (int i = 0; i < printable.size(); i++) {
            ArrayList<String> row = printable.get(i);
            printable2dArray[i] = row.toArray(new String[row.size()]);
        }

        final PrettyPrinter printer = new PrettyPrinter(System.out);
        printer.print(printable2dArray);
        System.out.println("");
    }
}
