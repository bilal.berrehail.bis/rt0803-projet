package com.project.model;

import java.util.ArrayList;

import com.project.App;
import com.project.utils.ScannerUtils;

public class Sommet {
    public static final String patternLabel = "[A-Za-z_0-9]+";

    private String label;
    private Integer[] D;
    private Sommet[] Nb;
    private Integer[][] ndis;

    private ArrayList<Sommet> neighbors;

    private Graph graph;

    private int i;

    public Sommet() {
    }

    public String getLabel() {
        return this.label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Integer[] getD() {
        return this.D;
    }

    public void setD(Integer[] D) {
        this.D = D;
    }

    public Sommet[] getNb() {
        return this.Nb;
    }

    public void setNb(Sommet[] Nb) {
        this.Nb = Nb;
    }

    public Integer[][] getNdis() {
        return this.ndis;
    }

    public void setNdis(Integer[][] ndis) {
        this.ndis = ndis;
    }

    public ArrayList<Sommet> getNeighbors() {
        return this.neighbors;
    }

    public void setNeighbors(ArrayList<Sommet> neighbors) {
        this.neighbors = neighbors;
    }

    public Graph getGraph() {
        return this.graph;
    }

    public void setGraph(Graph graph) {
        this.graph = graph;
    }

    public int getI() {
        return this.i;
    }

    public void setI(int i) {
        this.i = i;
    }

    @Override
    public String toString() {
        return label;
    }

    public void initialize() {
        for (int a = 0; a < neighbors.size(); a++) {
            for (int b = 0; b < graph.getSommets().size(); b++) {
                ndis[a][b] = graph.getN();
            }
        }

        for (int a = 0; a < graph.getSommets().size(); a++) {
            D[a] = graph.getN();
            Nb[a] = new SommetUndefined();
        }

        D[i] = 0;
        Nb[i] = this;
    }

    public void sendDistInitialized() {
        for (Sommet neighbor : neighbors) {

            App.displayVariables();
            System.out.println(String.format(">> [%s] Envoi (Mydist, %d, %d) à %s. (Appuyez sur entrée pour continuer)",
                    label, i, 0, neighbor.getLabel()));
            ScannerUtils.pressEnter();

            neighbor.receiveDist(i, 0, this);
        }
    }

    public void receiveDist(int v, int d, Sommet w) {
        System.out.println(
                String.format("<< [%s] Reception de (Mydist, %d, %d) d'un voisin %s.", label, v, d, w.getLabel()));
        ndis[w.getI()][v] = d;
        recompute(v);
    }

    public void recompute(int v) {
        Integer d_v_save = D[v];

        if (v == i) {
            D[v] = 0;
            Nb[v] = this;
        } else {
            Sommet w = null;
            Integer d = null;

            for (int w_index = 0; w_index < neighbors.size(); w_index++) {
                if (d == null) {
                    d = ndis[w_index][v];
                    w = neighbors.get(w_index);
                } else if (ndis[w_index][v] < d) {
                    d = ndis[w_index][v];
                    w = neighbors.get(w_index);
                }
            }

            d++;

            if (d < graph.getN()) {
                D[v] = d;
                Nb[v] = w;
            } else {
                D[v] = graph.getN();
                Nb[v] = new SommetUndefined();
            }
        }

        if (d_v_save != D[v]) {
            for (Sommet neighbor : neighbors) {

                App.displayVariables();
                System.out.println(
                        String.format(">> [%s] Envoi (Mydist, %d, %d) à %s. (Appuyez sur entrée pour continuer)", label,
                                v, D[v], neighbor.getLabel()));
                ScannerUtils.pressEnter();

                neighbor.receiveDist(v, D[v], this);
            }
        }
    }

    public void failCanal(Sommet w) {
        for (int v = 0; v < graph.getSommets().size(); v++) {
            recompute(v);
        }
    }

    public void repairCanal(Sommet w) {
        for (int v = 0; v < graph.getSommets().size(); v++) {
            ndis[w.getI()][v] = graph.getN();

            App.displayVariables();
            System.out.println(String.format(">> [%s] Envoi (Mydist, %d, %d) à %s. (Appuyez sur entrée pour continuer)",
                    label, v, D[v], graph.getSommets().get(v).getLabel()));
            ScannerUtils.pressEnter();

            graph.getSommets().get(v).receiveDist(v, D[v], this);
        }
    }
}