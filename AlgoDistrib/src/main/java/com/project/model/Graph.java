package com.project.model;

import java.util.ArrayList;

import com.project.utils.GraphUtils;

public class Graph {
    private ArrayList<Sommet> sommets;
    private ArrayList<Branch> branchs;
    private int n;

    public ArrayList<Sommet> getSommets() {
        return this.sommets;
    }

    public void setSommets(ArrayList<Sommet> sommets) {
        this.sommets = sommets;
    }

    public ArrayList<Branch> getBranchs() {
        return this.branchs;
    }

    public void setBranchs(ArrayList<Branch> branchs) {
        this.branchs = branchs;
    }

    public int getN() {
        return this.n;
    }

    public void setN(int n) {
        this.n = n;
    }

    public void runTajibnapis() {
        n = sommets.size();

        for (int i = 0; i < sommets.size(); i++) {
            sommets.get(i).setD(new Integer[n]);
            sommets.get(i).setNb(new Sommet[n]);
            sommets.get(i).setNdis(new Integer[n][n]);

            GraphUtils.setNeighborsForSommet(this, sommets.get(i));

            sommets.get(i).setGraph(this);

            sommets.get(i).setI(i);
        }

        for (Sommet sommet : sommets) {
            sommet.initialize();
        }

        for (Sommet sommet : sommets) {
            sommet.sendDistInitialized();
        }
    }
}