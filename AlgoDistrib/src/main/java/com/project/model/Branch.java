package com.project.model;

public class Branch {
    public static final String patternBranch = "--";

    private Sommet sommetA;
    private Sommet sommetB;

    private boolean broken;

    public Branch() {
        broken = false;
    }

    public Sommet getSommetA() {
        return this.sommetA;
    }

    public void setSommetA(Sommet sommetA) {
        this.sommetA = sommetA;
    }

    public Sommet getSommetB() {
        return this.sommetB;
    }

    public void setSommetB(Sommet sommetB) {
        this.sommetB = sommetB;
    }

    public boolean isBroken() {
        return this.broken;
    }

    public void setBroken(boolean broken) {
        this.broken = broken;
    }
}