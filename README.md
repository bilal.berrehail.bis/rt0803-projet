## Comment déployer notre application ?

* Clonez le dépôt : git clone https://gitlab.com/bilal.berrehail.bis/rt0803-projet.git

* Modifiez votre graphe en fonction de vos envies dans le fichier de configuration **graph.ini** présent dans le dossier JAR.

* Lancez un terminal et mettez-vous dans le dossier JAR et faîtes la commande : java -jar Algo.jar


